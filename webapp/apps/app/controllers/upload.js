const h = require('../helpers/commonHelper');
const uh = require('../helpers/uploadHelper');
const fs = require('fs');
const path = require('path');
const TASK_STATUS_NEW = "new";
const TASK_STATUS_INPROGRESS = "inprogress";
const TASK_STATUS_SUCCESS = "success";
const TASK_STATUS_ERROR = "error";

module.exports.getAll = async (req, res, next) => {
    let errors = [];
    if (!req.headers.token) {
        errors.push('header "token" is required');
    } else {
        if (!await h.isValidToken(req.headers.token)) {
            errors.push('invalid token');
            h.sendJsonResponse(res, 401, {errors: errors});
            return;
        }
    }

    if (errors.length) {
        h.sendJsonResponse(res, 406, {errors: errors});
        return;
    }

    uh.getUploads(req, res);
};

module.exports.getById = async (req, res, next) => {
    let errors = [];
    if (!req.headers.token) {
        errors.push('header "token" is required');
    } else {
        if (!await h.isValidToken(req.headers.token)) {
            errors.push('invalid token');
            h.sendJsonResponse(res, 401, {errors: errors});
            return;
        }
    }

    if (errors.length) {
        h.sendJsonResponse(res, 406, {errors: errors});
        return;
    }

    uh.getUpload(req, res);
};

module.exports.upload = (req, res, next) => {
    uh.upload(req, res);
};

module.exports.download = async (req, res, next) => {
    let errors = [];
    const downloadFile = path.resolve(__dirname + '/../uploads/' + req.params.id);

    console.log('?', downloadFile);

    try {
        if (fs.existsSync(downloadFile)) {
            //let filename;
            //const task = await th.getTaskById(req.params.id);

            uh.getUploadById(req, (err, uploadEx)=>{
                if(err){
                    errors.push('error reading file');
                    h.sendJsonResponse(res, 500, err);
                    return;
                }

                if(!uploadEx){
                    errors.push('error reading file');
                    h.sendJsonResponse(res, 500, errors);
                    return;
                }

                res.download(downloadFile, uploadEx.title);

            });
        } else {
            errors.push('error reading file');
            h.sendJsonResponse(res, 404, {errors: errors});
        }

    } catch (err) {
        errors.push('error reading file');
        h.sendJsonResponse(res, 404, {errors: errors});

    }
};

module.exports.delete = async (req, res, next) => {

    let errors = [];
    if (!req.headers.token) {
        errors.push('header "token" is required');
    } else {
        if (!await h.isValidToken(req.headers.token)) {
            errors.push('invalid token');
            h.sendJsonResponse(res, 401, {errors: errors});
            return;
        }
    }

    if (errors.length) {
        h.sendJsonResponse(res, 500, {errors: errors});
        return;
    }

    uh.delete(req, res);
};
