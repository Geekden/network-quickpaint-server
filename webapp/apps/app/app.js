const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
require('./models/db');
const logger = require('morgan');

const authRouter = require('./routes/auth');
const uploadRouter = require('./routes/upload');

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/', express.static(path.join(__dirname, 'public')));

app.use('/api', authRouter);
app.use('/api', uploadRouter);

//The 404 Route
app.get('*', function(req, res){
    res.status = 404;
    res.send('');
});

module.exports = app;
