/**
 * показывает в строке состояния сообщение message
 */
function showMessage(message) {
	clearMessage();
	var bm = document.querySelector('#block-messages');
	bm.innerHTML = message;
	bm.classList.remove('hide');
	bm.classList.add('success');
}

/**
 * показывает в строке состояния ошибку error
 */
function showError(error) {
	clearMessage();
	var bm = document.querySelector('#block-messages');
	bm.innerHTML = error;
	bm.classList.remove('hide');
	bm.classList.add('error');
}

/**
 * очищает строку состояния
 */
function clearMessage() {
	var bm = document.querySelector('#block-messages');
	bm.classList.remove('success');
	bm.classList.remove('error');
	bm.classList.add('hide');
}

/**
 * коллбэк для скрытия каждого блока. Вызывается в forEach
 */
function hideEachBlock(blockId) {
	// DOM -- Document Object Model
	var b = document.querySelector('#' + blockId);
	// console.log('b = ', b);
	b.classList.add('hide');
}

/**
 * скрывает все блоки
 */
function hideAllBlocks() {
	var blocks = ['block-signup', 'block-login', 'block-logout', 'block-selfremove', 'block-files', 'block-delete-file'];
	blocks.forEach(hideEachBlock);
}

/**
 * Осуществляет переход на страницу регистрации
 */
function gotoSignupPage() {
	hideAllBlocks();
	document.querySelector('#block-signup').classList.remove('hide');
	document.querySelector('#block-signup').classList.add('container-login');
}

/**
 * Осуществляет переход на страницу логина
 */
function gotoLoginPage() {
	hideAllBlocks();
	document.querySelector('#block-login').classList.remove('hide');
}

/**
 * загрузка файла
 */
function uploadFile() {
	var myHeaders = new Headers();
	myHeaders.append("token", localStorage.getItem('appToken'));

	var fileName = document.querySelector('#btn-upload').files[0].name;

	var formdata = new FormData();
	formdata.append("file", document.querySelector('#btn-upload').files[0], fileName);
	formdata.append("title", fileName);
	formdata.append("body", "QuickPaint Body");

	var requestOptions = {
		method: 'POST',
		headers: myHeaders,
		body: formdata,
		redirect: 'follow'
	};

	fetch("/api/upload/", requestOptions)
		.then(function (response) {
			return response.json();
		})
		.then(function (result) {
			if (result.errors) {
				showError('Ошибка: ' + result.errors.join(', '));
				setTimeout(function () {
					clearMessage();
				}, 8000);

			} else {
				hideAllBlocks();
				updateTable(function () {
					initTableButtons();
					document.querySelector('#block-files').classList.remove('hide');
					showMessage('Файл: ' + fileName + ' успешно загружен');
				});
			}
		})
		.catch(function (error) {
			showError('Ошибка: ' + error);
			setTimeout(function () {
				clearMessage();
			}, 8000);
		});
}

/**
 * Получает dataset (который содержит id и title от загрузки)
 * Делает запрос к серверу на получение файла по id и выдаёт полученный файл на загрузку с именем title
 */
function downloadFile(dataset) {

	var data, filename, type;

	var myHeaders = new Headers();
	myHeaders.append("token", localStorage.getItem('appToken'));

	var requestOptions = {
		method: 'GET',
		headers: myHeaders,
		redirect: 'follow'
	};

	fetch("/api/download/" + dataset.id, requestOptions)
		.then(function (response) {
			return response.blob();
		}).then(function (result) {
			var blob = result;
			var url = window.URL.createObjectURL(blob);
			var a = document.createElement('a');
			a.href = url;
			a.download = dataset.title;
			document.body.appendChild(a);
			a.click();
			a.remove();
		})
		.catch(function (error) {
			showError('Ошибка: ' + error);
			setTimeout(function () {
				clearMessage();
			}, 8000);
		});
}

/**
 * Получает dataset (который содержит id и title от загрузки)
 * Делает запрос к серверу на удаление файла по id и удаляет его из списка загрузок
 */
function deleteFile(dataset) {
	var myHeaders = new Headers();
	myHeaders.append("token", localStorage.getItem('appToken'));

	var requestOptions = {
		method: 'DELETE',
		headers: myHeaders,
		redirect: 'follow'
	};

	fetch("/api/upload/" + dataset.id, requestOptions)
		.then(response => response.status)
		.then(function (result) {
			console.log(result);
			if (result == '204') {
				hideAllBlocks();
				updateTable(function () {
					initTableButtons();
					document.querySelector('#block-files').classList.remove('hide');
					showMessage('Файл: ' + dataset.title + ' был удален');
				});

			} else {
				//FIX добавить оброаботку ошибок 

				// if(result == '401'){
				// localStorage.removeItem('appToken');
				// localStorage.removeItem('appLogin');
				// hideAllBlocks();
				// showMessage('Успешный выход из системы');
				// document.querySelector('#block-login').classList.remove('hide');
				// console.log('I am token. I slip');
				// } else {
				console.log('Chto tbl takoe', result);

				//}
				setTimeout(function () {
					clearMessage();
				}, 8000);
			}
			setTimeout(function () {
				clearMessage()
			}, 5000);
		})
		.catch(error => console.log('error', error));
}

/**
 * очищает слушателей событий клика на кнопках в таблице (если они есть)
 * повторно добавляет слуштели события клика на кнопки таблицы
 */
function initTableButtons() {
	// отбираем на DOM-дереве все кнопки из таблицы в блоке #block-files
	var btns = document.querySelectorAll('#block-files table button');
	// проходим по всем отобранным кнопкам и выполняем действия через коллбэк 
	btns.forEach(function (tableBtn) {

		function onClickTableBtn(event) {
			var dataset = event.target.dataset;
			var classCheck = event.target.classList.value;
			switch (classCheck) { //проверяем класс чтобы разветвить действия по кнопкам
				case 'btn btn-accent':
					console.log('download detected = ', dataset);
					downloadFile(dataset);
					break;
				case 'btn btn-danger':
					alert("Вы хотите удалить файл?" + dataset.title);
					deleteFile(dataset);
					// console.log('delete detected = ', dataset);
					// hideAllBlocks();
					// document.querySelector('#block-delete-file').classList.remove('hide');
					break;
			}
		}

		tableBtn.removeEventListener('click', onClickTableBtn);
		tableBtn.addEventListener('click', onClickTableBtn);

	});

	document.querySelector('#btn-upload').addEventListener('change', uploadFile);
}

/**
 * получает данные от сервера и обновляет таблицу
 */
function updateTable(cb) {

	showMessage('Подоэждите, мы получаем данные от сервера...');

	var myHeaders = new Headers();
	myHeaders.append("token", localStorage.getItem('appToken'));

	var requestOptions = {
		method: 'GET',
		headers: myHeaders,
	};

	fetch("/api/uploads", requestOptions)
		.then(function (response) {
			return response.json();
		})
		.then(function (result) {

			if (result.errors) {
				if (result.errors[0] === 'invalid token') {
					showError('Время сессии истекло... Введите данные снова...');
					setTimeout(function () {
						gotoLoginPage();
					}, 1000);
				} else {
					console.log('ошибки?', result.errors);
					showError('Ошибка: ' + result.errors.join(', '));
				}

				setTimeout(function () {
					clearMessage();
				}, 8000);

			} else {
				var rowsArray = [];

				result.forEach(function (eachRow) {
					rowsArray.push(`
					<tr>
					<td>${eachRow.title}</td>
					<td>${eachRow.body}</td>
					<td>
					<button data-id="${eachRow._id}" data-title="${eachRow.title}" class="btn btn-accent">Скачать файл</button>
					<button data-id="${eachRow._id}" data-title="${eachRow.title}" class="btn btn-danger">Удалить файл</button>
					</td>
					</tr>`);
				})

				var rows = rowsArray.join('');

				var markup = `
			<table cellpadding="0" cellspacing="0">
			<thead>
			<tr>
			<th>Название</th>
			<th>Описание</th>
			<th>Действия</th>
			</tr>
			</thead>

			<tbody>
			${rows}
			</tbody>
			</table>	
			`;

				document.querySelector('#block-files-table').innerHTML = markup;

				showMessage('Данные получены');
				setTimeout(function () {
					clearMessage();
					cb();
				}, 1000);

			}

			// fixxx
		}).catch(function (error) {
			showError('Ошибка: ' + error);
			setTimeout(function () {
				clearMessage();
			}, 8000);
		});
}


/**
 * коллбэк для обработки события клика на кнопке
 */
function onClickBtn(event) {

	hideAllBlocks();

	var actionType = event.target.id;

	switch (actionType) {
		case 'btn-signup':
			document.querySelector('#block-signup').classList.remove('hide');
			document.querySelector('#block-signup').classList.add('container-login');
			break;
		case 'btn-login':
			document.querySelector('#block-login').classList.remove('hide');
			document.querySelector('#block-signup').classList.add('container-login');
			break;
		case 'btn-update':
			updateTable(function () {
				initTableButtons();
				document.querySelector('#block-files').classList.remove('hide');
			});

			break;
		case 'btn-logout':
			document.querySelector('#block-logout').classList.remove('hide');
			break;

		case 'btn-logout-ok':
			logout();
			break;

		case 'btn-selfremove':
			document.querySelector('#block-selfremove').classList.remove('hide');
			break;

		case 'btn-selfremove-ok':
			deleteAccount();
			break;

		case 'btn-delete-ok':
			deleteFile(dataset);
			break;

		case 'btn-show-message':
			showMessage('успешное сообщение');
			break;

		case 'btn-show-error':
			showError('сообщение ошибки');
			break;

		case 'btn-clear-message':
			clearMessage();
			break;

	}


}

/**
 * Получает форму. Анализирует все дочерние элементы формы form
 * Отбирает дочерние элементы типа INPUT, имеющие аттрибут name, в массив fields и возвращает их.
 */
function getFormFields(form) {
	var fields = [];

	var formChildrens = form.children;

	for (var i = formChildrens.length - 1; i >= 0; i--) {
		var children = formChildrens[i];
		if (children.tagName === 'INPUT' && children.hasAttribute('name')) {
			fields.push({
				name: children.name,
				value: children.value
			});
		}
	}
	return fields;
}

/**
 * функция валидации формы логина
 */
function formLoginValidate(fields) {

	clearMessage();

	var isValid = true;
	var errors = [];

	fields.forEach(function (fied) {

		if (fied.name === 'login') {
			var checkLogin = fied.value.length >= 3;
			isValid = isValid && checkLogin;
			if (!checkLogin) {
				errors.push('Логин должен быть мининмум 3 символа');
			}
		} else if (fied.name === 'password') {
			var checkPassword = fied.value.length >= 3;
			isValid = isValid && checkPassword;
			if (!checkPassword) {
				errors.push('Длина пароля должна быть миниму 3 символа');
			}
		}

	});

	if (errors.length) {
		showError('Ошибка заполнения формы: ' + errors.join(', '));
		setTimeout(function () {
			clearMessage();
		}, 5000);
	}

	return isValid;
}

/**
 * функция отправки формы регистрации(только в случае успешной валидации)
 */
function formSignupSubmit(fields) {

	var login, password;

	fields.forEach(function (fied) {
		if (fied.name === 'password') {
			password = fied.value;
		} else if (fied.name === 'login') {
			login = fied.value;
		}
	});

	localStorage.setItem('appLogin', login)

	var myHeaders = new Headers();
	myHeaders.append("Content-Type", "application/json");

	var raw = JSON.stringify({
		"login": login,
		"password": password
	});

	var requestOptions = {
		method: 'POST',
		headers: myHeaders,
		body: raw,
		redirect: 'follow'
	};

	fetch("/api/signup", requestOptions)
		.then(function (response) {
			return response.json();
		})
		.then(function (result) {

			if (result.errors) {
				showError('Ошибка: ' + result.errors.join(', '));
				setTimeout(function () {
					clearMessage();
				}, 8000);

			} else {
				hideAllBlocks();
				gotoLoginPage();
				showMessage('Аккаунт создан. Введите данные снова.');
				setTimeout(function () {}, 8000);
			}


		})
		.catch(function (error) {
			showError('Ошибка: ' + error);
			setTimeout(function () {
				clearMessage();
			}, 8000);
		});
}

/**
 * функция отправки формы логина (только в случае успешной валидации)
 */
function formLoginSubmit(fields) {
	var login, password;
	fields.forEach(function (fied) {
		if (fied.name === 'login') {
			login = fied.value;
		} else if (fied.name === 'password') {
			password = fied.value;
		}
	});

	localStorage.setItem('appLogin', login);

	var myHeaders = new Headers();
	myHeaders.append("Content-Type", "application/json");

	var raw = JSON.stringify({
		"login": login,
		"password": password
	});

	var requestOptions = {
		method: 'POST',
		headers: myHeaders,
		body: raw,
		redirect: 'follow'
	};

	fetch("/api/login", requestOptions)
		.then(function (response) {
			return response.json();
		})
		.then(function (result) {

			if (result.errors) {
				if (result.errors.join(', ') == 'account does not exist') {
					gotoSignupPage();
					showError('Кажется такого аккаунта нет... Вы можете его создать.');
					setTimeout(function () {
						clearMessage();
					}, 8000);
				} else {
					showError('Ошибка: ' + result.errors.join(', '));
					setTimeout(function () {
						clearMessage();
					}, 8000);
				}

			} else {
				localStorage.setItem('appToken', result.token);
				hideAllBlocks();
				updateTable(function () {
					document.querySelector('#block-files').classList.remove('hide');
					showMessage('Здравствуйте, ' + login);
					initTableButtons();
				});
			}


		})
		.catch(function (error) {
			showError('Ошибка: ' + error);
			setTimeout(function () {
				clearMessage();
			}, 8000);
		});

}

/**
 * Отсылает запрос на сервер на удаление пользователя. Затем затирает токен в локальном хранилище
 */
function deleteAccount() {
	var token = localStorage.getItem('appToken');
	var login = localStorage.getItem('appLogin');

	var myHeaders = new Headers();
	myHeaders.append("Content-Type", "application/json");
	myHeaders.append("token", token);

	var requestOptions = {
		method: 'DELETE',
		headers: myHeaders,
		redirect: 'follow'
	};

	fetch("/api/deleteaccount/" + login, requestOptions)
		.then(response => response.status)
		.then(function (result) {
			if (result == '204') {
				localStorage.removeItem('appToken');
				localStorage.removeItem('appLogin');
				hideAllBlocks();
				showMessage('Аккаунт: ' + login + ' был удален -_-');
				document.querySelector('#block-login').classList.remove('hide');

			} else {
				//FIX добавить оброаботку ошибок 

				// if(result == '401'){
				// localStorage.removeItem('appToken');
				// localStorage.removeItem('appLogin');
				// hideAllBlocks();
				// showMessage('Успешный выход из системы');
				// document.querySelector('#block-login').classList.remove('hide');
				// console.log('I am token. I slip');
				// } else {
				console.log('Chto tbl takoe', result);

				//}
				setTimeout(function () {
					clearMessage();
				}, 8000);
			}
			setTimeout(function () {
				clearMessage()
			}, 5000);
		})
		.catch(error => console.log('error', error));
}

/**
 * Отсылает запрос на сервер на выход пользователя. Затем затирает токен в локальном хранилище
 */
function logout() {
	var token = localStorage.getItem('appToken');
	var login = localStorage.getItem('appLogin');

	var myHeaders = new Headers();
	myHeaders.append("Content-Type", "application/json");
	myHeaders.append("token", token);

	var requestOptions = {
		method: 'GET',
		headers: myHeaders,
		redirect: 'follow'
	};

	fetch("/api/logout/" + login, requestOptions)
		.then(function (response) {
			return response.json();
		})
		.then(function (result) {

			if (result.errors) {
				showError('Ошибка: ' + result.errors.join(', '));
				setTimeout(function () {
					clearMessage();
				}, 8000);

			} else {
				if (result.status && result.status === 'success') {
					localStorage.removeItem('appToken');
					localStorage.removeItem('appLogin');
					hideAllBlocks();
					showMessage('Прощайте, ' + login);
					document.querySelector('#block-login').classList.remove('hide');
				} else {
					showError('Кажется что-то пошло не так...');
				}

			}


		})
		.catch(function (error) {
			showError('Ошибка: ' + error);
			setTimeout(function () {
				clearMessage();
			}, 8000);
		});

}


/**
 * коллбэк для обработки события клика на кнопке
 */
function onSubmitForm(event) {

	event.preventDefault();

	var actionType = event.target.id;

	switch (actionType) {

		case 'signup':
			var signupFormFields = getFormFields(event.target);

			if (formLoginValidate(signupFormFields)) {
				formSignupSubmit(signupFormFields);
			}
			break;

		case 'login':
			var loginFormFields = getFormFields(event.target);

			if (formLoginValidate(loginFormFields)) {
				formLoginSubmit(loginFormFields);
			}
			break;

			/*case '':
			break;*/
	}

	// console.log('onSubmitForm DETECTED! actionType = ', actionType);

}


/**
 * коллбэк для инициализации нажатий на все кнопки.
 * Вызывается при переборе всех button.btn для того, чтобы прослушать на них событие клика
 */
function withEachBtn(btn) {
	btn.addEventListener('click', onClickBtn);
}


/**
 * коллбэк для инициализации отправки всех форм.
 * Вызывается при переборе всех form.dataform для того, чтобы прослушать на них событие отправки
 */
function withEachForm(form) {
	form.addEventListener('submit', onSubmitForm);
}

/**
 * функция для демонстрации работы механизма обещаний (promise)
 */
function promiseTest() {
	console.log('promiseTest detected!');

	function p1Callback(resolve, reject) {
		setTimeout(function () {
			reject('error case');
		}, 6000);

		setTimeout(function () {
			resolve('success case');
		}, 7000);

	}

	var p1 = new Promise(p1Callback);

	console.log('p1 now = ', p1);

	p1.then(function (value) {
		console.log('p1 success. value === ', value);
	}).catch(function (error) {
		console.log('p1 error. error === ', error);
	});

}

if (localStorage.getItem('appToken')) {
	hideAllBlocks();
	updateTable(function () {
		document.querySelector('#block-files').classList.remove('hide');
		showMessage('Здравствуйте, ' + localStorage.getItem('appLogin'));
		initTableButtons();
	});
}