// КНОПКИ

// отбираем на DOM-дереве все кнопки с классом btn
var btns = document.querySelectorAll('button.btn');
// проходим по всем отобранным кнопкам и выполняем действия через коллбэк withEachBtn
btns.forEach(withEachBtn);



// ФОРМЫ

// отбираем на DOM-дереве все формы с классом dataform
var forms = document.querySelectorAll('form.dataform');
// проходим по всем отобранным формам и выполняем действия через коллбэк withEachForm
forms.forEach(withEachForm);