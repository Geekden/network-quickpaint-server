let mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);

const uploadSchema = mongoose.Schema({
    userId: {type: String, required: true},
    title: {type: String, default: 'New Title'},
    body: {type: String}
});

mongoose.model('upload', uploadSchema);
