const express = require('express');
const router = express.Router();
const ctrl = require('../controllers/upload');
router.get('/uploads', ctrl.getAll);
router.get('/upload/:id', ctrl.getById);
router.post('/upload', ctrl.upload);
router.get('/download/:id', ctrl.download);
router.delete('/upload/:id', ctrl.delete);
module.exports = router;
