const mongoose = require('mongoose');
const user = mongoose.model('user');
const token = mongoose.model('token');
const uploadModel = mongoose.model('upload');
const multer = require('multer');
const upload = multer({dest: __dirname + '/../uploads'}).fields([{name: 'file', maxCount: 1}]);
const h = require('../helpers/commonHelper');
const exec = require('child_process').exec;
const fs = require('fs-extra');

module.exports.getUploadById = (req, cb) => {
    token.findOne({token: req.headers.token}, (err, tokenEx) => {
        if (err) {
            cb(err);
            return;
        }
        uploadModel.findOne({_id: req.params.id, userId: tokenEx.userId}, cb);
    });
};

module.exports.getUploads = (req, res) => {
    token.findOne({token: req.headers.token}, (err, tokenEx) => {
        if (err) {
            h.sendJsonResponse(res, 500, err);
            return;
        }
        uploadModel.find({userId: tokenEx.userId.toString()}, (err, uploads) => {
            if (err) {
                h.sendJsonResponse(res, 500, err);
                return;
            }
            h.sendJsonResponse(res, 200, uploads);
        });

    });
};

module.exports.getUpload = (req, res) => {
    token.findOne({token: req.headers.token}, (err, tokenEx) => {
        if (err) {
            h.sendJsonResponse(res, 500, err);
            return;
        }

        uploadModel.findOne({_id: req.params.id, userId: tokenEx.userId}, (err, upload) => {
            if (err) {
                h.sendJsonResponse(res, 500, err);
                return;
            }
            h.sendJsonResponse(res, 200, upload);
        });
    });
};

module.exports.upload = (req, res) => {

    let errors = [];

    upload(req, res, async (err) => {

        if (err instanceof multer.MulterError) {
            errors.push(err);
            h.sendJsonResponse(res, 501, {errors: errors});
            return;
        }

        if (!req.headers.token) {
            errors.push('header "token" is required');
        } else {
            if (!await h.isValidToken(req.headers.token)) {
                errors.push('invalid token');
                h.sendJsonResponse(res, 401, {errors: errors});
                return;
            }
        }

        if (!req.body.title) {
            errors.push('field "title" is required');
            h.sendJsonResponse(res, 500, {errors: errors});
            return;
        }

        if (!req.body.body) {
            errors.push('field "body" is required');
            h.sendJsonResponse(res, 500, {errors: errors});
            return;
        }

        if (undefined === req.files.file) {
            errors.push('no files detected');
            h.sendJsonResponse(res, 500, {errors: errors});
            return;
        }

        if (!req.files.file.length) {
            errors.push('no files detected');
            h.sendJsonResponse(res, 500, {errors: errors});
            return;
        }

        if (errors.length) {
            h.sendJsonResponse(res, 406, {errors: errors});
            return;
        }

        token.findOne({token: req.headers.token}, (err, tokenEx) => {
            if (err) {
                h.sendJsonResponse(req, 500, err);
                return;
            }

            const userId = tokenEx.userId;

            // create upload
            uploadModel.create({
                userId: userId,
                title: req.body.title,
                body: req.body.body,
            }, async (err, uploadEx) => {
                if (err) {
                    errors.push(err);
                    h.sendJsonResponse(res, 500, {errors: errors});
                    return;
                }

                const filename = req.files.file[0];
                const newFilename = [__dirname, '..', 'uploads', uploadEx._id].join('/');

                fs.copyFile(filename.path, newFilename, err => {
                    if (err) {
                        errors.push(err);
                        h.sendJsonResponse(req, 500, err);
                        return;
                    }
                    // remove tmp files
                    fs.unlink(filename.path, err => {
                        if (err) {
                            errors.push(err);
                            h.sendJsonResponse(req, 500, err);
                            return;
                        }
                        h.sendJsonResponse(res, 201, uploadEx);
                    });
                });
            });
        });
    });
};

module.exports.delete = (req, res) => {
    token.findOne({token: req.headers.token}, (err, tokenEx) => {
        if (err) {
            h.sendJsonResponse(res, 500, err);
            return;
        }

        uploadModel.deleteMany({_id: req.params.id, userId: tokenEx.userId}, err => {
            if (err) {
                h.sendJsonResponse(res, 500, err);
                return;
            }
            h.sendJsonResponse(res, 204, null);
        });
    });
};